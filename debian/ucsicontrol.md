% ucsicontrol(1) | libtypec documentation
# NAME

ucsicontrol - list UCSI connector details

# SYNOPSIS

**ucsicontrol** \[*OPTIONS*\]

# OPTIONS

**\--help**, **-h**

:   Show usage message

**\--conn_rst** *conn_num* **soft**|**hard**

:   Perform reset on the connector. Specify the type of the reset, **soft** or **hard**.

**\--get_cap**

:   Get capabilities

**\--get_conn_cap** *conn_num*

:   Get Connector Capability

**\--get_conn_sts** *conn_num*

:   Get Connector Status

**\--get_cable_prop** *conn_num*

:   Get Cable Properties

**\--get_cur_cam** *conn_num*

:   Get Current Alternate mode

**\--get_alt_mode** *conn_num* *recipient*
:   Get Alternate Modes

|      *recipient*: 0: connector, 1: SOP, 2: SOP', 3: SOP''

**\--get_pdos** *conn_num* *partner* *offset* *src_snk* *type*

:   Get PDOs from local and partner Policy Managers

|      *partner*: set to 1 to retrieve partner PDOs
|      *offset*: starting offset of the first PDO to be returned, 0 to 7
|      *src_snk*: set to 1 to retrieve source PDOs, 0 to retrieve sink PDOs
|      *type*: type of PDOs to retrieve (0: current supported, 1: advertised, 2: maximum supported)

**\--set_uor** *conn_num* **DFP**|**UFP**|**Accept**
:   Set the USB operation role for the connector to operate at.
If the connector does not have an active connection,
this command has no effect.

|      **DFP**: Downstream Facing Port
|      **UFP**: Upstream Facing Port
|      **Accept**: Accept role swap change requests from the port partner

**\--set_pdr** *conn_num* **SRC**|**SNK**|**Accept**
:   Set the Power direction that the OPM wants the connector to operate at.
If the connector does not have an active connection,
or the partner is not PD-capable, this command has no effect.

|      **SRC**: Source role
|      **SNK**: Sink role
|      **Accept**: Accept power swap change requests from the port partner

# BUGS

Error handling while parsing command-line argument is poor, expect crashed
when not passing all required arguments.

# AUTHORS

Madhu M <madhu.m@intel.com>, Rajaram Regupathy <rajaram.regupathy@gmail.com>

